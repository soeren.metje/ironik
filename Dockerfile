FROM python:3.10.9-slim

WORKDIR /app

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.3.2

# Install system deps
RUN pip install "poetry==$POETRY_VERSION"

# for base img python:3.10.9-slim (gcc needed for building netifaces)
RUN apt update \
    && apt install -y gcc \
    && rm -rf /var/lib/apt/lists/*

# Install project dependencies
COPY poetry.lock pyproject.toml ./

RUN poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi --no-root  # --no-root skips all checks regarding readme, src, etc

# Build project
COPY . .

# poetry install without --no-root beacause some setup step is apparently needed
RUN poetry install --no-interaction --no-ansi \
    && poetry build

CMD ironik
