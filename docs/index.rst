Welcome to ironik's documentation!
===================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   cli_usage
   manual_kubernetes_deployment
   authors
   code_of_conduct
   modules

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
