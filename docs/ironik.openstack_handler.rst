ironik.openstack\_handler package
=================================

Submodules
----------

ironik.openstack\_handler.openstack\_api\_caller module
-------------------------------------------------------

.. automodule:: ironik.openstack_handler.openstack_api_caller
   :members:
   :undoc-members:
   :show-inheritance:

ironik.openstack\_handler.resource\_calculator module
-----------------------------------------------------

.. automodule:: ironik.openstack_handler.resource_calculator
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ironik.openstack_handler
   :members:
   :undoc-members:
   :show-inheritance:
