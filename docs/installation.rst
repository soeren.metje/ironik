.. highlight:: shell

============
Installation
============


Stable release
--------------

To install ironik, run this command in your terminal:

.. code-block:: console

    $ pip install ironik

This is the preferred method to install ironik, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for ironik can be downloaded from the `Gitlab repo`_.
Please note that you require `poetry`_ to be installed.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://gitlab.gwdg.de/jonathan.decker1/ironik.git

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ make install


.. _Gitlab repo: https://gitlab.gwdg.de/jonathan.decker1/ironik
.. _poetry: https://python-poetry.org/
