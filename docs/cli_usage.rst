CLI Usage
=========

Setting up a Kubernetes cluster on OpenStack and Rancher requires a lot of configuration details.
Ironik uses a template file to hold said configurations that must be filled out before anything can be deployed.

First run the following command to receive a blank template:

.. code:: console

   $ ironik template

This creates the file `ironik_template.yaml` in the local directory.
Open this file with a text editor.

Filling out the template
########################

The template has multiple sections:
- deployment_options
- kubernetes_config
- network_config
- openstack_config
- openstack_credentials
- rancher_config
- rancher_credentials

Deployment options are used to configure the validation of the cluster once it is setup.
****************************************************************************************
This is not implemented yet and can be ignored for now.

Kubernetes config allows setting the number of master and worker nodes to deploy as well as what roles they should have.
************************************************************************************************************************
Furthermore, the version of Kubernetes can be set here.

`master_node_roles` lists all the roles the master nodes should have.
This can be mostly left as it is unless master nodes should, for example, not have the worker role.

`number_master_nodes` is the number of master nodes to deploy initially.
This can be changed later via Rancher.

`number_worker_nodes` is the number of worker nodes to deploy initially.
This can be changed later via Rancher.

`version` is the Kubernetes version to be deployed.
The version must be available in the Rancher instance used and exactly match the version string there.

`worker_node_roles` lists all the roles the worker nodes should have.
This should usually be left as just `worker`.

Network config describes what ports should be opened in the security group created in OpenStack.
**************************************************************************************************
It also allows setting the port range that Kubernetes is allowed to use.
These can be left as it is as the list of ports is based on the Rancher documentation and what ports it requires.

`required_TCP_ports` sets the TCP ports to open in the security group.
These should usually be left alone. Additional ports can later be opened in the security group in OpenStack.

`required_UDP_ports` sets the UDP ports to open in the security group.
These should usually be left alone. Additional ports can later be opened in the security group in OpenStack.

`worker_port_range_max` sets the high end of the ports that Kubernetes can use.
This is both set in the OpenStack security group as well as in the Kubernetes settings.

`worker_port_range_min` sets the low end of the ports that Kubernetes can use.
This is both set in the OpenStack security group as well as in the Kubernetes settings.

OpenStack config needs to be filled out for the deployment to work.
********************************************************************

`default_flavor_name` is the flavor to use for the nodes in OpenStack.
OpenStack web UI -> Compute -> Instances -> Launch Instance -> Flavor, for example: 'm1.medium'

`default_image_name` is the image to use for the nodes in OpenStack.
OpenStack web UI -> Compute -> Images, for example: 'Ubuntu 20.04.3 Server x86_64 (ssd)'

`lb_provider` is the load balancer system deployed in OpenStack.
OpenStack web UI -> Network -> Load Balancers -> Create a new load balancer and check what is set as its provider.
Common `lb_provider` are `amphora`, `octavia` or in older systems `haproxy`.

`openstack_auth_url` is the authentication endpoint to use for OpenStack.
OpenStack web UI -> API access -> Identity, append "/v3" to it.

`private_network_id` is the private network into which the nodes should be deployed.
OpenStack web UI -> Network -> select Networks -> pick a private network -> select overview -> ID

`project_domain_name` is the domain of the project in OpenStack.
OpenStack web UI -> Identity -> Projects -> Domain Name

`region_name` is the region used by OpenStack.
This is usually "RegionOne".

`remote_ip_prefix` is the IP mask for the IPs used inside the OpenStack network.
This can be for example `10.254.1.0/24`.

`security_group_name` sets the name of the security group in OpenStack to create.
If this groups already exist, it will be deleted and recreated.

`use_octavia` determines whether to use the Octavia load balancer in OpenStack.
Should Octavia not be installed, set this to `false`.

`user_domain_name` is the domain used when logging in to OpenStack.
Often its the same as the `project_domain_name`

`volume_size` sets the size of volumes to be created for each node in OpenStack.

Next are the OpenStack credentials.
*************************************

`password` is the OpenStack password used to login.
`project_id` is the OpenStack project id.

OpenStack web UI -> Identity -> Projects -> Project ID

`username` is the OpenStack username used to login.

Rancher config set configuration on for Rancher.
**************************************************

`engine_install_url` is the url from where the docker engine install script should be fetched.
This can be left as it is.

`new_cluster_admin_user_name` is the name of the user who should be admin of the new cluster in Rancher.
If the user does not exist, it will be created.

`new_cluster_admin_user_password` is the password of the user who should be admin of the new cluster.
If it is left empty, it will be created. If the user already exist, it will be ignored.

`rancher_api_base_url` is the url used to access the Rancher API.
It is commonly the base web address of the Rancher instance with "/v3" at the end.

`ssh_user`is the name of the default ssh user for new nodes in OpenStack.
This might be `root` or `cloud` depending on the OpenStack instance.

Rancher credentials are the Rancher API credentials.
*****************************************************

`rancher_access_key` is the access key part of the Rancher API key.

`rancher_secret_key` is the secret key part of the Rancher API key.
The Rancher API key must be unscoped and can be created via the Rancher web interface.

Starting deployment
####################

Once the template has been filled out it can be used to start a deployment.

.. code:: console

   $ ironik deploy cluster_name ./ironik_template.yaml

Replace `cluster_name` with the desired name for the new cluster.

Ironik will test out the credentials and check whether enough resources are available in OpenStack.
Then it reports said information in a table and asks for confirmation.

After confirming the start of the deployment it will create the node templates in Rancher,
the cluster in Rancher and wait for the cluster to deploy.

After the cluster is ready it will attempt to create a new user and print the generated password into the console if a new user was created.

Afterwards Ironik will apply the manifests used to install the OpenStack controller manager and the Cinder driver.
