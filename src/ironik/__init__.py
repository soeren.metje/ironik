"""Top-level package for ironik."""

__author__ = "Jonathan Decker"
__email__ = "jonathan.decker@uni-goettingen.de"
__version__ = "0.1.6"
